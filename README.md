# frabrunelle.com

> This repository contains the source files for [frabrunelle.com](https://frabrunelle.com/).

## Contributing

By contributing to this website, you dedicate your work to the public domain and relinquish any copyright claims under the terms of the [CC0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).

### Submitting a pull request

- [Fork this GitLab repository](https://gitlab.com/frabrunelle/frabrunelle.com/forks/new) and clone your fork locally on your computer.
- Install [Jekyll](https://jekyllrb.com/) and other dependencies with the command `make install`*
- Test out your changes with the command `make serve`
- When you're happy, push your changes up and open a pull request.

\* You should ensure you have [Bundler](https://bundler.io/) installed.

If you're not sure how to open a pull request, feel free to [open an issue](https://gitlab.com/frabrunelle/frabrunelle.com/issues/new) instead.

## Credits

This website uses the [Bootstrap](http://getbootstrap.com/) CSS framework.

## Public domain

Unless otherwise noted, all content on this website is [dedicated to the public domain](https://gitlab.com/frabrunelle/frabrunelle.com/blob/master/LICENSE) under the [CC0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
